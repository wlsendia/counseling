package com.wlsendia.counseling.controller;

import com.wlsendia.counseling.model.CustomerRequest;
import com.wlsendia.counseling.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer") // 관행적으로 클래스이름을 써서 만들음
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody CustomerRequest request) { // 이름 겹쳐도 ㄱㅊ, 바구니 이름이 리퀘스트
        customerService.setCustomer(request.getName(), request.getPhone()); // 고객에게 받아온 리퀘스트에서 네임, 폰 추출
        return "skwnddptjqltmwkrtjdRmxskrhwkrtjdgktldh";
    }
}
// 엔티티 레포지토리 모델 서비스 컨트롤러 순서