package com.wlsendia.counseling.repository;

import com.wlsendia.counseling.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository <Customer, Long> {
}
