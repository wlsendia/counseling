package com.wlsendia.counseling.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Customer {

    @Id // pk
    @GeneratedValue(strategy = GenerationType.IDENTITY) // pk 먼저 만들고 생각하기. 기본 양식
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    private String phone;
}
